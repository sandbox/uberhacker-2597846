<?php
/**
 * @file
 * Administration page callbacks.
 */

define('FLICKR_ALBUM_DEFAULTS', serialize(
  array(
    'alignment'         => 'center',        // topLeft, topCenter, topRight, centerLeft, center, centerRight, bottomLeft, bottomCenter, bottomRight
    'autoAdvance'       => true,            // true, false
    'mobileAutoAdvance' => true,            // true, false. Auto-advancing for mobile devices
    'barDirection'      => 'leftToRight',   // 'leftToRight', 'rightToLeft', 'topToBottom', 'bottomToTop'
    'barPosition'       => 'bottom',        // 'bottom', 'left', 'top', 'right'
    'cols'              => 6,
    'easing'            => 'easeInOutExpo', // for the complete list visit http://api.jqueryui.com/easings/
    'mobileEasing'      => '',              // leave empty if you want to display the same easing on mobile devices and on desktop etc.
    'fx'                => 'random',        // 'random','simpleFade', 'curtainTopLeft', 'curtainTopRight', 'curtainBottomLeft', 'curtainBottomRight', 'curtainSliceLeft', 'curtainSliceRight', 'blindCurtainTopLeft', 'blindCurtainTopRight', 'blindCurtainBottomLeft', 'blindCurtainBottomRight', 'blindCurtainSliceBottom', 'blindCurtainSliceTop', 'stampede', 'mosaic', 'mosaicReverse', 'mosaicRandom', 'mosaicSpiral', 'mosaicSpiralReverse', 'topLeftBottomRight', 'bottomRightTopLeft', 'bottomLeftTopRight'
                                            // you can also use more than one effect, just separate them with commas: 'simpleFade, scrollRight, scrollBottom'
    'mobileFx'          => '',              // leave empty if you want to display the same effect on mobile devices and on desktop etc.
    'gridDifference'    => 250,             // to make the grid blocks slower than the slices, this value must be smaller than transPeriod
    'height'            => '50%',           // here you can type pixels (for instance '300px'), a percentage (relative to the width of the slideshow, for instance '50%') or 'auto'
    'imagePath'         => 'images/',       // the path to the image folder (it serves for the blank.gif, when you want to display videos)
    'hover'             => true,            // true, false. Pause on state hover. Not available for mobile devices
    'loader'            => 'pie',           // pie, bar, none (even if you choose "pie", old browsers like IE8- can't display it... they will display always a loading bar)
    'loaderColor'       => '#eeeeee', 
    'loaderBgColor'     => '#222222', 
    'loaderOpacity'     => .8,              // 0, .1, .2, .3, .4, .5, .6, .7, .8, .9, 1
    'loaderPadding'     => 2,               // how many empty pixels you want to display between the loader and its background
    'loaderStroke'      => 7,               // the thickness both of the pie loader and of the bar loader. Remember: for the pie, the loader thickness must be less than a half of the pie diameter
    'minHeight'         => '200px',         // you can also leave it blank
    'navigation'        => true,            // true or false, to display or not the navigation buttons
    'navigationHover'   => true,            // if true the navigation button (prev, next and play/stop buttons) will be visible on hover state only, if false they will be visible always
    'mobileNavHover'    => true,            // same as above, but only for mobile devices
    'opacityOnGrid'     => false,           // true, false. Decide to apply a fade effect to blocks and slices: if your slideshow is fullscreen or simply big, I recommend to set it false to have a smoother effect 
    'overlayer'         => true,            // a layer on the images to prevent the users grab them simply by clicking the right button of their mouse (.camera_overlayer)
    'pagination'        => true,
    'playPause'         => true,            // true or false, to display or not the play/pause buttons
    'pauseOnClick'      => true,            // true, false. It stops the slideshow when you click the sliders.
    'pieDiameter'       => 38,
    'piePosition'       => 'rightTop',      // 'rightTop', 'leftTop', 'leftBottom', 'rightBottom'
    'portrait'          => false,           // true, false. Select true if you don't want your images cropped
    'rows'              => 4,
    'slicedCols'        => 12,              // if 0 the same value of cols
    'slicedRows'        => 8,               // if 0 the same value of rows
    'slideOn'           => 'random',        // next, prev, random: decide if the transition effect will be applied to the current (prev) or the next slide
    'thumbnails'        => false,
    'time'              => 7000,            // milliseconds between the end of the sliding effect and the start of the next one
    'transPeriod'       => 1500,            // length of the sliding effect in milliseconds
    'cache_expiration'  => CACHE_PERMANENT,
  )
));

define('FLICKR_ALBUM_MAP', serialize(
  array(
    'alignment'           => 'alignment',
	'auto_advance'        => 'autoAdvance',
	'mobile_auto_advance' => 'mobileAutoAdvance',
	'bar_direction'       => 'barDirection',
	'bar_position'        => 'barPosition',
	'cols'                => 'cols',
	'easing'              => 'easing',
	'mobile_easing'       => 'mobileEasing',
	'fx'                  => 'fx',
	'mobile_fx'           => 'mobileFx',
	'grid_difference'     => 'gridDifference',
	'height'              => 'height',
	'image_path'          => 'imagePath',
	'hover'               => 'hover',
	'loader'              => 'loader',
	'loader_color'        => 'loaderColor',
	'loader_bg_color'     => 'loaderBgColor',
	'loader_opacity'      => 'loaderOpacity',
	'loader_padding'      => 'loaderPadding',
	'loader_stroke'       => 'loaderStroke',
	'min_height'          => 'minHeight',
	'navigation'          => 'navigation',
	'navigation_hover'    => 'navigationHover',
	'mobile_nav_hover'    => 'mobileNavHover',
	'opacity_on_grid'     => 'opacityOnGrid',
	'overlayer'           => 'overlayer',
	'pagination'          => 'pagination',
	'play_pause'          => 'playPause',
	'pause_on_click'      => 'pauseOnClick',
	'pie_diameter'        => 'pieDiameter',
	'pie_position'        => 'piePosition',
	'portrait'            => 'portrait',
	'rows'                => 'rows',
	'sliced_cols'         => 'slicedCols',
	'sliced_rows'         => 'slicedRows',
	'slide_on'            => 'slideOn',
	'thumbnails'          => 'thumbnails',
	'time'                => 'time',
	'trans_period'        => 'transPeriod',
	'cache_expiration'    => 'cache_expiration',
  )
));

/**
 * Administration settings form.
 */
function flickr_album_settings_form() {
  $form = array();

  drupal_set_title(t('Flickr Album Settings'));

  $form['flickr_album_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Flickr API Key'),
    '#description' => t('Enter the Flickr API Key for photosets.'),
    '#default_value' => variable_get('flickr_album_api_key', ''),
    '#required' => TRUE,
  );
  $form['flickr_album_cache_expiration'] = array(
    '#type' => 'textfield',
    '#title' => t('Cache expiration (sec)'),
    '#description' => t('Duration between cache expirations (in seconds).  Minimum recommended value is 3600.'),
    '#default_value' => variable_get('flickr_album_cache_expiration', flickr_album_get_default('cache_expiration')),
    '#size' => 8,
    '#maxlength' => 8,
    '#required' => TRUE,
  );
  $form['flickr_album_display'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $fx_options = array(
    'random'                  => t('Random'),
    'simpleFade'              => t('Simple Fade'),
    'curtainTopLeft'          => t('Curtain Top Left'),
    'curtainTopRight'         => t('Curtain Top Right'),
    'curtainBottomLeft'       => t('Curtain Bottom Left'),
    'curtainBottomRight'      => t('Curtain Bottom Right'),
    'curtainSliceLeft'        => t('Curtain Slice Left'),
    'curtainSliceRight'       => t('Curtain Slice Right'),
    'blindCurtainTopLeft'     => t('Blind Curtain Top Left'),
    'blindCurtainTopRight'    => t('Blind Curtain Top Right'),
    'blindCurtainBottomLeft'  => t('Blind Curtain Bottom Left'),
    'blindCurtainBottomRight' => t('Blind Curtain Bottom Right'),
    'blindCurtainSliceBottom' => t('Blind Curtain Slice Bottom'),
    'blindCurtainSliceTop'    => t('Blind Curtain Slice Top'),
    'stampede'                => t('Stampede'),
    'mosaic'                  => t('Mosaic'),
    'mosaicReverse'           => t('Mosaic Reverse'),
    'mosaicRandom'            => t('Mosaic Random'),
    'mosaicSpiral'            => t('Mosaic Spiral'),
    'mosaicSpiralReverse'     => t('Mosaic Spiral Reverse'),
    'topLeftBottomRight'      => t('Top Left Bottom Right'),
    'bottomRightTopLeft'      => t('Bottom Right Top Left'),
    'bottomLeftTopRight'      => t('Bottom Left Top Right'),
  );
  $form['flickr_album_display']['flickr_album_fx'] = array(
    '#type' => 'select',
    '#title' => t('Effects'),
    '#default_value' => variable_get('flickr_album_fx', flickr_album_get_default('fx')),
    '#options' => $fx_options,
  );
  $easing_options = array(
    'linear'           => t('Linear'),
    'swing'            => t('Swing'),
    'easeInQuad'       => t('Ease In Quad'),
    'easeOutQuad'      => t('Ease Out Quad'),
    'easeInOutQuad'    => t('Ease In/Out Quad'),
    'easeInCubic'      => t('Ease In Cubic'),
    'easeOutCubic'     => t('Ease Out Cubic'),
    'easeInOutCubic'   => t('Ease In/Out Cubic'),
    'easeInQuart'      => t('Ease In Quart'),
    'easeOutQuart'     => t('Ease Out Quart'),
    'easeInOutQuart'   => t('Ease In/Out Quart'),
    'easeInQuint'      => t('Ease In Quint'),
    'easeOutQuint'     => t('Ease Out Quint'),
    'easeInOutQuint'   => t('Ease In/Out Quint'),
    'easeInExpo'       => t('Ease In Expo'),
    'easeOutExpo'      => t('Ease Out Expo'),
    'easeInOutExpo'    => t('Ease In/Out Expo'),
    'easeInSine'       => t('Ease In Sine'),
    'easeOutSine'      => t('Ease Out Sine'),
    'easeInOutSine'    => t('Ease In/Out Sine'),
    'easeInCirc'       => t('Ease In Circle'),
    'easeOutCirc'      => t('Ease Out Circle'),
    'easeInOutCirc'    => t('Ease In/Out Circle'),
    'easeInElastic'    => t('Ease In Elastic'),
    'easeOutElastic'   => t('Ease Out Elastic'),
    'easeInOutElastic' => t('Ease In/Out Elastic'),
    'easeInBack'       => t('Ease In Back'),
    'easeOutBack'      => t('Ease Out Back'),
    'easeInOutBack'    => t('Ease In/Out Back'),
    'easeInBounce'     => t('Ease In Bounce'),
    'easeOutBounce'    => t('Ease Out Bounce'),
    'easeInOutBounce'  => t('Ease In/Out Bounce'),
  );
  $form['flickr_album_display']['flickr_album_easing'] = array(
    '#type' => 'select',
    '#title' => t('Easing'),
    '#description' => t('Easing method to use for animations.'),
    '#default_value' => variable_get('flickr_album_easing', flickr_album_get_default('easing')),
    '#options' => $easing_options,
  );
  $form['flickr_album_display']['flickr_album_navigation'] = array(
    '#type' => 'checkbox',
    '#title' => t('Navigation'),
    '#description' => t('Check to display the navigation buttons.'),
    '#default_value' => variable_get('flickr_album_navigation', flickr_album_get_default('navigation')),
  );
  $form['flickr_album_display']['flickr_album_navigation_hover'] = array(
    '#type' => 'checkbox',
    '#title' => t('Navigation Hover'),
    '#description' => t('Check to display the navigation buttons (prev, next and play/stop buttons) on hover state only for desktops. If unchecked, the navigation will always be displayed.'),
    '#default_value' => variable_get('flickr_album_navigation_hover', flickr_album_get_default('navigation_hover')),
  );
  $form['flickr_album_display']['flickr_album_opacity_on_grid'] = array(
    '#type' => 'checkbox',
    '#title' => t('Opacity On Grid'),
    '#description' => t('Check to apply a fade effect to blocks and slices. If your slideshow is fullscreen or simply big, I recommend to uncheck for a smoother effect.'),
    '#default_value' => variable_get('flickr_album_opacity_on_grid', flickr_album_get_default('opacity_on_grid')),
  );
  $form['flickr_album_display']['flickr_album_overlayer'] = array(
    '#type' => 'checkbox',
    '#title' => t('Over Layer'),
    '#description' => t('Check for a layer on the images to prevent users from grabbing them simply by clicking their right mouse button.'),
    '#default_value' => variable_get('flickr_album_overlayer', flickr_album_get_default('overlayer')),
  );
  $form['flickr_album_display']['flickr_album_pagination'] = array(
    '#type' => 'checkbox',
    '#title' => t('Pagination'),
    '#description' => t('Check to enable pagination.'),
    '#default_value' => variable_get('flickr_album_pagination', flickr_album_get_default('pagination')),
  );
  $form['flickr_album_display']['flickr_album_play_pause'] = array(
    '#type' => 'checkbox',
    '#title' => t('Play/Pause'),
    '#description' => t('Check to display the play/pause buttons.'),
    '#default_value' => variable_get('flickr_album_play_pause', flickr_album_get_default('play_pause')),
  );
  $form['flickr_album_display']['flickr_album_pause_on_click'] = array(
    '#type' => 'checkbox',
    '#title' => t('Pause On Click'),
    '#description' => t('Check to stop the slideshow when you click the sliders.'),
    '#default_value' => variable_get('flickr_album_pause_on_click', flickr_album_get_default('pause_on_click')),
  );
  $form['flickr_album_display']['flickr_album_auto_advance'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto Advance'),
    '#description' => t('Check to enable auto advance.'),
    '#default_value' => variable_get('flickr_album_auto_advance', flickr_album_get_default('auto_advance')),
  );
  $form['flickr_album_display']['flickr_album_hover'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hover'),
    '#description' => t('Check to pause on state hover. Not available for mobile devices.'),
    '#default_value' => variable_get('flickr_album_hover', flickr_album_get_default('hover')),
  );
  $form['flickr_album_display']['flickr_album_portrait'] = array(
    '#type' => 'checkbox',
    '#title' => t('Portrait'),
    '#description' => t('Check to prevent images from cropping.'),
    '#default_value' => variable_get('flickr_album_portrait', flickr_album_get_default('portrait')),
  );
  $form['flickr_album_display']['flickr_album_thumbnails'] = array(
    '#type' => 'checkbox',
    '#title' => t('Thumbnails'),
    '#description' => t('Check to display thumbnails.'),
    '#default_value' => variable_get('flickr_album_thumbnails', flickr_album_get_default('thumbnails')),
  );
  $form['flickr_album_align'] = array(
    '#type' => 'fieldset',
    '#title' => t('Alignment'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $alignment_options = array(
    'topLeft'      => t('Top Left'),
    'topCenter'    => t('Top Center'),
    'topRight'     => t('Top Right'),
    'centerLeft'   => t('Left Center'),
    'center'       => t('Center'),
    'centerRight'  => t('Center Right'),
    'bottomLeft'   => t('Bottom Left'),
    'bottomCenter' => t('Bottom Center'),
    'bottomRight'  => t('Bottom Right'),
  );
  $form['flickr_album_align']['flickr_album_alignment'] = array(
    '#type' => 'select',
    '#title' => t('Alignment'),
    '#default_value' => variable_get('flickr_album_alignment', flickr_album_get_default('alignment')),
    '#options' => $alignment_options,
  );
  $direction_options = array(
    'leftToRight' => t('Left to Right'),
    'rightToLeft' => t('Right to Left'),
    'topToBottom' => t('Top to Bottom'),
    'bottomToTop' => t('Bottom to Top'),
  );
  $form['flickr_album_align']['flickr_album_bar_direction'] = array(
    '#type' => 'select',
    '#title' => t('Bar Direction'),
    '#default_value' => variable_get('flickr_album_bar_direction', flickr_album_get_default('bar_direction')),
    '#options' => $direction_options,
  );
  $position_options = array(
    'bottom' => t('Bottom'),
    'left'   => t('Left'),
    'top'    => t('Top'),
    'right'  => t('Right'),
  );
  $form['flickr_album_align']['flickr_album_bar_position'] = array(
    '#type' => 'select',
    '#title' => t('Bar Position'),
    '#default_value' => variable_get('flickr_album_bar_position', flickr_album_get_default('bar_position')),
    '#options' => $position_options,
  );
  $form['flickr_album_align']['flickr_album_slide_on'] = array(
    '#type' => 'select',
    '#title' => t('Slide On'),
    '#description' => t('Select which transition effect will be applied to the current (prev) or next slide.'),
    '#default_value' => variable_get('flickr_album_slide_on', flickr_album_get_default('slide_on')),
    '#options' => array('prev' => t('Prev'), 'next' => t('Next'), 'random' => t('Random')),
  );
  $position_options = array(
	'leftTop'     => t('Left Top'),
    'rightTop'    => t('Right Top'),
	'leftBottom'  => t('Left Bottom'),
	'rightBottom' => t('Right Bottom'),
  );
  $form['flickr_album_align']['flickr_album_pie_position'] = array(
    '#type' => 'select',
    '#title' => t('Pie Position'),
    '#default_value' => variable_get('flickr_album_pie_position', flickr_album_get_default('pie_position')),
    '#options' => $position_options,
  );
  $form['flickr_album_dimensions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Dimensions'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['flickr_album_dimensions']['flickr_album_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#description' => t('Album height (in pixels, a percentage relative to the width or auto).'),
    '#default_value' => variable_get('flickr_album_height', flickr_album_get_default('height')),
    '#size' => 5,
    '#maxlength' => 5,
    '#required' => TRUE,
  );
  $form['flickr_album_dimensions']['flickr_album_min_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum height'),
    '#description' => t('Enter the minimum height (in pixels). This can also be blank.'),
    '#default_value' => variable_get('flickr_album_min_height', flickr_album_get_default('min_height')),
    '#size' => 5,
    '#maxlength' => 5,
  );
  $form['flickr_album_dimensions']['flickr_album_cols'] = array(
    '#type' => 'textfield',
    '#title' => t('Columns'),
    '#default_value' => variable_get('flickr_album_cols', flickr_album_get_default('cols')),
    '#size' => 3,
    '#maxlength' => 3,
    '#required' => TRUE,
  );
  $form['flickr_album_dimensions']['flickr_album_sliced_cols'] = array(
    '#type' => 'textfield',
    '#title' => t('Sliced Columns'),
    '#description' => t('Enter 0 for the same number of columns as above.'),
    '#default_value' => variable_get('flickr_album_sliced_cols', flickr_album_get_default('sliced_cols')),
    '#size' => 3,
    '#maxlength' => 3,
    '#required' => TRUE,
  );
  $form['flickr_album_dimensions']['flickr_album_rows'] = array(
    '#type' => 'textfield',
    '#title' => t('Rows'),
    '#default_value' => variable_get('flickr_album_rows', flickr_album_get_default('rows')),
    '#size' => 3,
    '#maxlength' => 3,
    '#required' => TRUE,
  );
  $form['flickr_album_dimensions']['flickr_album_sliced_rows'] = array(
    '#type' => 'textfield',
    '#title' => t('Sliced Rows'),
    '#description' => t('Enter 0 for the same number of rows as above.'),
    '#default_value' => variable_get('flickr_album_sliced_rows', flickr_album_get_default('sliced_rows')),
    '#size' => 3,
    '#maxlength' => 3,
    '#required' => TRUE,
  );
  $form['flickr_album_dimensions']['flickr_album_pie_diameter'] = array(
    '#type' => 'textfield',
    '#title' => t('Pie Diameter (px)'),
    '#description' => t('Enter the pie diameter (in pixels).'),
    '#default_value' => variable_get('flickr_album_pie_diameter', flickr_album_get_default('pie_diameter')),
    '#size' => 3,
    '#maxlength' => 3,
    '#required' => TRUE,
  );
  $form['flickr_album_load'] = array(
    '#type' => 'fieldset',
    '#title' => t('Loader'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['flickr_album_load']['flickr_album_loader'] = array(
    '#type' => 'select',
    '#title' => t('Loader'),
    '#description' => t('Even if you select "Pie", old browsers like IE8 cannot display it and will display a loading bar instead.'),
    '#default_value' => variable_get('flickr_album_loader', flickr_album_get_default('loader')),
    '#options' => array('pie' => t('Pie'), 'bar' => t('Bar'), 'none'  => t('None')),
  );
  $form['flickr_album_load']['flickr_album_loader_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Loader Color'),
    '#description' => t('Enter the hex color code for the loader.'),
    '#default_value' => variable_get('flickr_album_loader_color', flickr_album_get_default('loader_color')),
    '#size' => 7,
    '#maxlength' => 7,
    '#required' => TRUE,
  );
  $form['flickr_album_load']['flickr_album_loader_bg_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Loader Background Color'),
    '#description' => t('Enter the hex color code for the loader background.'),
    '#default_value' => variable_get('flickr_album_loader_bg_color', flickr_album_get_default('loader_bg_color')),
    '#size' => 7,
    '#maxlength' => 7,
    '#required' => TRUE,
  );
  $form['flickr_album_load']['flickr_album_loader_opacity'] = array(
    '#type' => 'textfield',
    '#title' => t('Loader Opacity'),
    '#description' => t('Enter the loader opacity. This value must be between 0 and 1.'),
    '#default_value' => variable_get('flickr_album_loader_opacity', flickr_album_get_default('loader_opacity')),
    '#size' => 4,
    '#maxlength' => 4,
    '#required' => TRUE,
  );
  $form['flickr_album_load']['flickr_album_loader_padding'] = array(
    '#type' => 'textfield',
    '#title' => t('Loader Padding (px)'),
    '#description' => t('Enter how many empty pixels you want to display between the loader and its background.'),
    '#default_value' => variable_get('flickr_album_loader_padding', flickr_album_get_default('loader_padding')),
    '#size' => 2,
    '#maxlength' => 2,
    '#required' => TRUE,
  );
  $form['flickr_album_load']['flickr_album_loader_stroke'] = array(
    '#type' => 'textfield',
    '#title' => t('Loader Stroke (px)'),
    '#description' => t('Enter the thickness in pixels of both the pie and bar loader. For the pie, the loader thickness must be less than half the pie diameter.'),
    '#default_value' => variable_get('flickr_album_loader_stroke', flickr_album_get_default('loader_stroke')),
    '#size' => 3,
    '#maxlength' => 3,
    '#required' => TRUE,
  );
  $form['flickr_album_timer'] = array(
    '#type' => 'fieldset',
    '#title' => t('Timer'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['flickr_album_timer']['flickr_album_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Delay (ms)'),
    '#description' => t('Time between the end of the sliding effect and the start of the next one (in milliseconds).'),
    '#default_value' => variable_get('flickr_album_time', flickr_album_get_default('time')),
    '#size' => 4,
    '#maxlength' => 4,
    '#required' => TRUE,
  );
  $form['flickr_album_timer']['flickr_album_trans_period'] = array(
    '#type' => 'textfield',
    '#title' => t('Transition Period (ms)'),
    '#description' => t('Enter the length of the sliding effect (in milliseconds).'),
    '#default_value' => variable_get('flickr_album_trans_period', flickr_album_get_default('trans_period')),
    '#size' => 4,
    '#maxlength' => 4,
    '#required' => TRUE,
  );
  $form['flickr_album_timer']['flickr_album_grid_difference'] = array(
    '#type' => 'textfield',
    '#title' => t('Grid Difference (ms)'),
    '#description' => t('To make the grid blocks slower than the slices, this value must be smaller than Transition Period.'),
    '#default_value' => variable_get('flickr_album_grid_difference', flickr_album_get_default('grid_difference')),
    '#size' => 4,
    '#maxlength' => 4,
    '#required' => TRUE,
  );
  $form['flickr_album_mobile'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mobile'),
    '#description' => t('Mobile device settings.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['flickr_album_mobile']['flickr_album_mobile_fx'] = array(
    '#type' => 'select',
    '#title' => t('Effects'),
    '#default_value' => variable_get('flickr_album_mobile_fx', flickr_album_get_default('mobile_fx')),
    '#options' => $fx_options,
  );
  $form['flickr_album_mobile']['flickr_album_mobile_auto_advance'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto Advance'),
    '#default_value' => variable_get('flickr_album_mobile_auto_advance', flickr_album_get_default('mobile_auto_advance')),
  );
  $form['flickr_album_mobile']['flickr_album_mobile_easing'] = array(
    '#type' => 'select',
    '#title' => t('Easing'),
    '#default_value' => variable_get('flickr_album_mobile_easing', flickr_album_get_default('mobile_easing')),
    '#options' => $easing_options,
  );
  $form['flickr_album_mobile']['flickr_album_mobile_nav_hover'] = array(
    '#type' => 'checkbox',
    '#title' => t('Navigation Hover'),
    '#description' => t('Check to display the navigation buttons (prev, next and play/stop buttons) on hover state only for mobile devices. If unchecked, the navigation will always be displayed.'),
    '#default_value' => variable_get('flickr_album_mobile_nav_hover', flickr_album_get_default('mobile_nav_hover')),
  );
  $form['flickr_album_debug'] = array(
    '#type' => 'fieldset',
    '#title' => t('Debug'),
    '#description' => t('Use this section to assist with troubleshooting the Flickr API.'),
    '#collapsible' => TRUE,
    '#collapsed' => !variable_get('flickr_album_response', ''),
  );
  $form['flickr_album_debug']['flickr_album_request'] = array(
    '#type' => 'textfield',
    '#title' => t('Flickr Photoset Request'),
    '#description' => t('Enter the Flickr Photoset ID. This is located in the url of your photoset after /sets/.<br />
                         Example: https://www.flickr.com/photos/utaustin/sets/72157645015476941/'),
  );
  $form['flickr_album_debug']['flickr_album_response'] = array(
    '#type' => 'textarea',
    '#title' => t('Flickr Photoset Response'),
    '#rows' => 30,
  );
  $form['flickr_album_restore_defaults'] = array(
    '#type' => 'submit',
    '#name' => 'flickr_album_restore_defaults',
    '#title' => t('Restore defaults'),
    '#value' => t('Restore defaults'),
  );

  $form['#validate'][] = 'flickr_album_settings_form_validate';
  $form['#submit'][] = 'flickr_album_settings_form_submit';

  return system_settings_form($form);
}

/**
 * Administration settings form validator.
 */
function flickr_album_settings_form_validate($form, &$form_state) {
  if ($api_key = $form_state['values']['flickr_album_api_key']) {
    if ($response = file_get_contents('https://api.flickr.com/services/rest/?method=flickr.test.echo&api_key=' . $api_key . '&format=php_serial')) {
      $response_array = unserialize($response);
      if (isset($response_array['stat']) && ($response_array['stat'] != 'ok')) {
        form_set_error('flickr_album_api_key', t('Not a valid Flickr API Key.'));
      }
    }
  }
  $cols = $form_state['values']['flickr_album_cols'];
  if (!flickr_album_valid_integer($cols)) {
    form_set_error('flickr_album_cols', t('Columns must be an integer value greater than 0.'));
  }
  $cols = $form_state['values']['flickr_album_sliced_cols'];
  if (!flickr_album_valid_integer($cols, TRUE)) {
    form_set_error('flickr_album_sliced_cols', t('Sliced columns must be an integer value greater than 0.'));
  }
  $rows = $form_state['values']['flickr_album_rows'];
  if (!flickr_album_valid_integer($rows)) {
    form_set_error('flickr_album_rows', t('Rows must be an integer value greater than 0.'));
  }
  $rows = $form_state['values']['flickr_album_sliced_rows'];
  if (!flickr_album_valid_integer($rows, TRUE)) {
    form_set_error('flickr_album_sliced_rows', t('Sliced rows must be an integer value greater than 0.'));
  }
  $loader = $form_state['values']['flickr_album_loader_padding'];
  if (!flickr_album_valid_integer($loader)) {
    form_set_error('flickr_album_loader_padding', t('Loader Padding must be an integer value greater than 0.'));
  }
  $loader = $form_state['values']['flickr_album_loader_stroke'];
  if (!flickr_album_valid_integer($loader)) {
    form_set_error('flickr_album_loader_stroke', t('Loader Stroke must be an integer value greater than 0.'));
  }
  $time = $form_state['values']['flickr_album_time'];
  if (!flickr_album_valid_integer($time)) {
    form_set_error('flickr_album_time', t('Delay must be an integer value greater than 0.'));
  }
  $period = $form_state['values']['flickr_album_trans_period'];
  if (!flickr_album_valid_integer($period)) {
    form_set_error('flickr_album_trans_period', t('Transition Period must be an integer value greater than 0.'));
  }
  $expiration = $form_state['values']['flickr_album_cache_expiration'];
  if (!flickr_album_valid_integer($expiration, TRUE)) {
    form_set_error('flickr_album_cache_expiration', t('Cache expiration must be an integer value greater than or equal to 0.'));
  }
  if ($photoset_id = $form_state['values']['flickr_album_request']) {
    if ($response = theme('flickr_album_photoset', array('photoset_id' => $photoset_id))) {
      $form_state['input']['flickr_album_response'] = $response;
      $form_state['rebuild'] = TRUE;
    }
    else {
      form_set_error('flickr_album_request', t('Unable to retrieve Flickr response or not a valid Flickr Photoset ID.'));
    }
  }
}

/**
 * Administration settings form submit.
 */
function flickr_album_settings_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#name'] == 'flickr_album_restore_defaults') {
    $elements = array('values', 'input');
    foreach ($elements as $element) {
      foreach ($form_state['values'] as $key => $value) {
        $default_key = substr($key, 13);
        $default_value = flickr_album_get_default($default_key);
        if ($default_value !== '') {
          $form_state[$element][$key] = $default_value;
        }
      }
    }
    $form_state['rebuild'] = TRUE;
  }
}

/**
 * Integer validator function.
 */
function flickr_album_valid_integer($value, $zero_allowed = FALSE) {
  $int_value = (string) (int) $value;
  if ($zero_allowed) {
    return !(!is_numeric($value) || ($value !== $int_value) || ($value < 0));
  }
  else {
    return !(!is_numeric($value) || ($value !== $int_value) || ($value <= 0));
  }
}

/**
 * Returns the default option value.
 */
function flickr_album_get_default($key) {
  $map = unserialize(FLICKR_ALBUM_MAP);
  if (isset($map[$key])) {
    $defaults = unserialize(FLICKR_ALBUM_DEFAULTS);
    return isset($defaults[$map[$key]]) ? $defaults[$map[$key]] : '';
  }
  else {
    return '';
  }
}
