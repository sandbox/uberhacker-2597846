INTRODUCTION
------------
The Flickr Album module animates Flickr photosets and provides
a text format filter to embed the slideshow in content areas.
The slideshow is fully responsive and configurable with a
plethora of options.

INSTALLATION
------------
1. Unpack the flickr_album folder and contents in the appropriate
   modules directory of your Drupal installation.

2. Download the Camera jQuery plugin and install in
   sites/all/libraries/Camera.  The plugin is available at
   https://github.com/pixedelic/Camera.  Simply git clone in
   sites/all/libraries.

3. Enable the Flickr Album module on the Modules admin page.

CONFIGURATION
-------------
1. Navigate to People > Permissions and scroll down to the Flickr Album
   section where you can assign the appropriate roles with the
   Administer Flickr Album settings permission.

2. Navigate to Configuration > Media > Flickr Album and enter your
   Flickr API Key and optionally set any additional preferences.
   If you don't already have a Flickr API Key, you can obtain one at
   https://www.flickr.com/services/apps/create/apply.

3. Navigate to Configuration > Content Authoring > Text formats and configure
   those you would like to apply the 'Flickr Album' filter. You'll need to
   adjust the filter processing order and place the Flickr Album filter near
   the top of your filter stack above 'Convert URLs into links' and below
   'Limit allowed HTML tags'.

4. By using the syntax [flickr-album:URL] in your content area, this filter
   will embed the Flickr photoset and provide a fully functional slideshow.
   The format of the URL is https://www.flickr.com/photos/myalbum/sets/#/show.
   Replace myalbum with your photoset location and # with your photoset id.
   You may override the default width and height values above by adding the
   filter using the following format: [flickr-album:URL width:X height:Y].
   If you omit any of these parameters, the default values will be used.
