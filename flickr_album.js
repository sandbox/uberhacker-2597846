/**
 * @file
 * JavaScript to control the Flickr Album view.
 */
(function ($) {
  Drupal.behaviors.flickrAlbum = {
    attach: function (context) {
      $(".flickr-album").camera({
//alert(toString(Drupal.settings.flickr_album_camera.alignment));
        // Camera Options
        alignment: Drupal.settings.flickr_album_camera.alignment,
        autoAdvance: parseInt(Drupal.settings.flickr_album_camera.autoAdvance),
        mobileAutoAdvance: parseInt(Drupal.settings.flickr_album_camera.mobileAutoAdvance),
        barDirection: Drupal.settings.flickr_album_camera.barDirection,
        barPosition: Drupal.settings.flickr_album_camera.barPosition,
        cols: parseInt(Drupal.settings.flickr_album_camera.cols),
        easing: Drupal.settings.flickr_album_camera.easing,
        mobileEasing: Drupal.settings.flickr_album_camera.mobileEasing,
        fx: Drupal.settings.flickr_album_camera.fx,
        mobileFx: Drupal.settings.flickr_album_camera.mobileFx,
        gridDifference: parseInt(Drupal.settings.flickr_album_camera.gridDifference),
        height: Drupal.settings.flickr_album_camera.height,
        imagePath: Drupal.settings.flickr_album_camera.imagePath,
        hover: parseInt(Drupal.settings.flickr_album_camera.hover),
        loader: Drupal.settings.flickr_album_camera.loader,
        loaderColor: Drupal.settings.flickr_album_camera.loaderColor,
        loaderBgColor: Drupal.settings.flickr_album_camera.loaderBgColor,
        loaderOpacity: Drupal.settings.flickr_album_camera.loaderOpacity,
        loaderPadding: Drupal.settings.flickr_album_camera.loaderPadding,
        loaderStroke: Drupal.settings.flickr_album_camera.loaderStroke,
        minHeight: Drupal.settings.flickr_album_camera.minHeight,
        navigation: parseInt(Drupal.settings.flickr_album_camera.navigation),
        navigationHover: parseInt(Drupal.settings.flickr_album_camera.navigationHover),
        mobileNavHover: parseInt(Drupal.settings.flickr_album_camera.mobileNavHover),
        opacityOnGrid: parseInt(Drupal.settings.flickr_album_camera.opacityOnGrid),
        overlayer: parseInt(Drupal.settings.flickr_album_camera.overlayer),
        pagination: parseInt(Drupal.settings.flickr_album_camera.pagination),
        playPause: parseInt(Drupal.settings.flickr_album_camera.playPause),
        pauseOnClick: parseInt(Drupal.settings.flickr_album_camera.pauseOnClick),
        pieDiameter: parseInt(Drupal.settings.flickr_album_camera.pieDiameter),
        piePosition: Drupal.settings.flickr_album_camera.piePosition,
        portrait: parseInt(Drupal.settings.flickr_album_camera.portrait),
        rows: parseInt(Drupal.settings.flickr_album_camera.rows),
        slicedCols: parseInt(Drupal.settings.flickr_album_camera.slicedCols),
        slicedRows: parseInt(Drupal.settings.flickr_album_camera.slicedRows),
        slideOn: parseInt(Drupal.settings.flickr_album_camera.slideOn),
        thumbnails: parseInt(Drupal.settings.flickr_album_camera.thumbnails),
        time: parseInt(Drupal.settings.flickr_album_camera.time),
        transPeriod: parseInt(Drupal.settings.flickr_album_camera.transPeriod)
      });
    }
  }
})(jQuery);
